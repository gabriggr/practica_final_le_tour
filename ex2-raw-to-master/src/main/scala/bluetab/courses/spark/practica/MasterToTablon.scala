package bluetab.courses.spark.practica

import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.util.Try

trait MasterToTablon {


  // TODO: Hay que implementar esta función
  /**
    * Esta función se tiene que utilizar para leer los tablones de master
    * @param masterTable
    * @return
    */
  def readMaster(masterTable: String): Try[DataFrame] = Try {
    ???
  }


  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función carga el tablon del tour uniendo todos los tablones de Master.
    *
    * @param spark sesión de spark
    * @return df DataFrame con los datos cargados
    */
  def loadTablonTour(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }

}
