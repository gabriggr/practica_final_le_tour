package bluetab.courses.spark.practica

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

import scala.util.{Failure, Success, Try}

trait CreateTablesMaster {

  private val logger = Logger.getLogger(this.getClass)

  val hqlFun = List(
    createMasterSchemaHQL _,
    createMasterTeamsTableHQL _,
    createMasterRidersTableHQL _,
    createMasterStagesTableHQL _,
    createMasterResultTableHQL _,
    createMasterTourTableHQL _
  )

  def executeDDL(expr: String)(implicit spark: SparkSession): Boolean = {
    Try {
      logger.info(expr)
      spark
        .sql(expr)
    } match {
      case Success(_) => {
        logger.info("Se ha ejecutado la HIVE correctamente");
        true
      }
      case Failure(ex) => {
        logger.error("error En la ejecución de la HIVE", ex);
        false
      }
    }
  }

  def createMasterSchemaHQL: String =
    """CREATE DATABASE IF NOT EXISTS tour_master;
      |""".stripMargin


  def createMasterTeamsTableHQL: String =
    """CREATE TABLE IF NOT EXISTS tour_master.teams
      |(
      | team String,
      | rider String,
      | id int
      | )
      | PARTITIONED BY (year int)
      |STORED AS parquet;
      |"""
      .stripMargin

  // TODO: Hay que implementar este método
  def createMasterRidersTableHQL: String = ""

  // TODO: Hay que implementar este método
  def createMasterStagesTableHQL: String = ""

  // TODO: Hay que implementar este método
  def createMasterResultTableHQL: String = ""

  // TODO: Hay que implementar este método
  def createMasterTourTableHQL: String = ""


}
