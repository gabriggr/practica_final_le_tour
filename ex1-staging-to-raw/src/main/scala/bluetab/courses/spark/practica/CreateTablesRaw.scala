package bluetab.courses.spark.practica


import org.apache.log4j.Logger
import org.apache.spark.sql.{SparkSession}

import scala.util.{Failure, Success, Try}

trait CreateTablesRaw {

  private val logger = Logger.getLogger(this.getClass)

  val hqlFun = List(
    createRawSchemaHQL _,
    createRawTeamsTableHQL _,
    createRawRidersTableHQL _,
    createRawStagesTableHQL _,
    createRawResultTableHQL _
  )

  def executeDDL(expr: String)(implicit spark: SparkSession): Boolean = {
    Try {
      logger.info(expr)
      spark
        .sql(expr)
    } match {
      case Success(_) => {
        logger.info("Se ha ejecutado la HIVE correctamente");
        true
      }
      case Failure(ex) => {
        logger.error("error En la ejecución de la HIVE", ex);
        false
      }
    }
  }

  def createRawSchemaHQL: String = "CREATE DATABASE IF NOT EXISTS tour_raw"


  def createRawTeamsTableHQL: String =
    """CREATE TABLE IF NOT EXISTS tour_raw.teams
      | (team String, rider String, id String, year String)
      | PARTITIONED BY (in_date int)
      | STORED AS avro""".stripMargin

  // TODO: Hay que implementar este método con la sentecia hql crear la tabla
  def createRawRidersTableHQL: String = ""

  // TODO: Hay que implementar este método con la sentecia hql crear la tabla
  def createRawStagesTableHQL: String = ""

  // TODO: Hay que implementar este método con la sentecia hql crear la tabla
  def createRawResultTableHQL: String = ""


}
