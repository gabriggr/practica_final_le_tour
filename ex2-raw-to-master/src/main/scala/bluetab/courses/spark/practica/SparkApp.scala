package bluetab.courses.spark.practica

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

trait SparkApp extends App {
  val logger = Logger.getLogger(this.getClass)
  implicit lazy val spark = {
    val session =
      SparkSession
        .builder
        .enableHiveSupport
        .getOrCreate

    session.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    session.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    session
  }
}
