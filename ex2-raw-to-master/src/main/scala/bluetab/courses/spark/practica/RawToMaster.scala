package bluetab.courses.spark.practica

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

import scala.util.Try

trait RawToMaster {


  def readRaw(rawTable: String, inDate: Int)(implicit spark: SparkSession): Try[DataFrame] = Try {
    spark
      .table(rawTable)
      .filter(col("in_date") === inDate)
  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los equipos de la capa de RAW en la capa de MASTER
    *
    * @param dfRaw dataFrame
    * @param spark sesión de spark
    * @return df DataFrame con los datos cargados
    */
  def loadTeamsFromRaw(dfRaw: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }


  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los ciclistas de la capa de RAW en la capa de MASTER
    *
    * @param dfRaw dataFrame
    * @param spark sesión de spark
    * @return df DataFrame con los datos cargados
    */
  def loadRidersFromRaw(dfRaw: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los etapas de la capa de RAW en la capa de MASTER
    *
    * @param dfRaw dataFrame
    * @param spark sesión de spark
    * @return df DataFrame con los datos cargados
    */
  def loadStagesFromRaw(dfRaw: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los resultados de la capa de RAW en la capa de MASTER
    *
    * @param dfRaw dataFrame
    * @param spark sesión de spark
    * @return df DataFrame con los datos cargados
    */
  def loadResultFromRaw(dfRaw: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }


}
