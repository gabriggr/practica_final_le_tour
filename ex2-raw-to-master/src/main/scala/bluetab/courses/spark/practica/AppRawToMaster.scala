package bluetab.courses.spark.practica

import scala.util.{Failure, Success}

object AppRawToMaster extends SparkApp with CreateTablesMaster with RawToMaster with MasterToTablon {

  // Get in_data from program params
  val inDate =
    if (args.length == 1) args(0).toInt
    else throw new IllegalArgumentException("Missing input param in_date")

  logger.info(s"in_date: $inDate")

  // 1. Create master database and tables
  hqlFun.forall(f => executeDDL(f()))

  // 2. Store Master data
  val rawTables = List(
    (TableRawTeams, loadTeamsFromRaw _),
    (TableRawRiders, loadRidersFromRaw _),
    (TableRawStages, loadStagesFromRaw _),
    (TableRawResult, loadResultFromRaw _)
  )

  val isLoadMaster =
  // Lee data frame y encadena con la carga de la tabla
    rawTables.forall(f => readRaw(f._1, inDate).flatMap(f._2(_)) match {
      case Success(_) => {
        logger.info(s"Process end Ok ${f._1}")
        true
      }
      case Failure(s) => {
        logger.error(s"Failed.${f._1} Reason: $s")
        false
      }
    })

  // 3. Store Tablon Tour data
  if (isLoadMaster) loadTablonTour match {
    case Success(_) => {
      logger.info(s"Process loadTablonTour end Ok ")
      true
    }
    case Failure(s) => {
      logger.error(s"Failed loadTablonTour Reason: $s")
      false
    }
  }

}
