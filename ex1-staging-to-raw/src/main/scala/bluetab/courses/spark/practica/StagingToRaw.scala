package bluetab.courses.spark.practica

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

import scala.util.Try

trait StagingToRaw {
  private val logger = Logger.getLogger(this.getClass)

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los equipos de la capa de STAGING en la capa de RAW
    *
    * @param path Path del fichero de teams
    * @param spark
    * @return df DataFrame con los datos cargados
    */
  def loadTeams(path: String, inDate: Int)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???

  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los ciclistas de la capa de STAGING en la capa de RAW
    *
    * @param path Path del fichero de riders
    * @param spark
    * @return df DataFrame con los datos cargados
    */
  def loadRiders(path: String, inDate: Int)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los equipos de la capa de STAGING en la capa de RAW
    *
    * @param path Path del fichero de teams
    * @param spark
    * @return df DataFrame con los datos cargados
    */
  def loadStages(path: String, inDate: Int)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }

  // TODO: Hay que implementar esta función
  /**
    *
    * Esta función tiene que cargar los datos de los Resultados de las carreras de la capa de STAGING en la capa de RAW
    *
    * @param path Path del fichero de result
    * @param spark
    * @return df DataFrame con los datos cargados
    */
  def loadResult(path: String, inDate: Int)(implicit spark: SparkSession): Try[DataFrame] = Try {
    ???
  }


}
