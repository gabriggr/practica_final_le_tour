# Ejercicio Final de Evaluación #

Este proyecto contiene el ejercicio final de evaluación del curso de Spark.

## Para descargar el proyecto ##
```sh

	git clone git@bitbucket.org:gabriggr/practica_final_le_tour.git
```

## Módulos ##

El proyecto contiene 4 módulos cada uno de ellos hace referencia a un ejercicio. Todos los ejercicios son dependiente unos con otros
por tanto es necesario que los módulos sean implementados en orden.

#### Módulo 1 - ex1-staging-to-raw ####
Contiene las templates de código necesarias para realizar el paso de los datos de la capa de Staging a Raw.

* [AppStagingToRaw](ex1-staging-to-raw/src/main/scala/bluetab/courses/spark/practica/AppStagingToRaw.scala)
* [CreateTablesRaw](ex1-staging-to-raw/src/main/scala/bluetab/courses/spark/practica/CreateTablesRaw.scala)
* [SparkApp](ex1-staging-to-raw/src/main/scala/bluetab/courses/spark/practica/SparkApp.scala)
* [StagingToRaw](ex1-staging-to-raw/src/main/scala/bluetab/courses/spark/practica/StagingToRaw.scala)

#### Módulo 2 - ex2-raw-to-master ####
Contiene las templates de código necesarias para realizar el paso de los datos de la capa de Raw a Master y la generación del tablón "tablon_tour".

* [AppRawToMaster](ex2-raw-to-master/src/main/scala/bluetab/courses/spark/practicaAppRawToMaster.scala)
* [CreateTablesMaster](ex2-raw-to-master/src/main/scala/bluetab/courses/spark/practica/CreateTablesMaster.scala)
* [MasterToTablon](ex2-raw-to-master/src/main/scala/bluetab/courses/spark/practica/MasterToTablon.scala)
* [RawToMaster](ex2-raw-to-master/src/main/scala/bluetab/courses/spark/practica/RawToMaster.scala)
* [SparkApp](ex2-raw-to-master/src/main/scala/bluetab/courses/spark/practica/SparkApp.scala)

#### Módulo 3 - ex3-create-output-general-ranking ####
Contiene las templates de código necesarias para realizar el ejercicio de generación de fichero de salida con la clasificación general de cada edicción.

* [AppGeneralRnk](ex3-create-output-general-ranking/src/main/scala/bluetab/courses/spark/practica/AppGeneralRnk.scala)
* [GeneralRnk](ex3-create-output-general-ranking/src/main/scala/bluetab/courses/spark/practica/GeneralRnk.scala)
* [SparkApp](ex3-create-output-general-ranking/src/main/scala/bluetab/courses/spark/practica/SparkApp.scala)

#### Módulo 4 - ex4-create-output-tour-metrics ####
Contiene las templates de código necesarias para realizar el ejercicio de generación de fichero de salida con la métricas de cada edición.

* [AppTourMetrics](ex4-create-output-tour-metrics/src/main/scala/bluetab/courses/spark/practica/AppTourMetrics.scala)
* [TourMetrics](ex4-create-output-tour-metrics/src/main/scala/bluetab/courses/spark/practica/TourMetrics.scala)
* [SparkApp](ex4-create-output-tour-metrics/src/main/scala/bluetab/courses/spark/practica/SparkApp.scala)

## Enunciado de la práctica ##

[Enunciado PDF](practica_final_2.pdf)