package bluetab.courses.spark.practica

import scala.util.{Failure, Success}

/**
  *
  * Parámetros para la ejecución
  * Paramétros VM:
  * -Dspark.master=local[*]
  */
object AppStagingToRaw extends SparkApp with StagingToRaw with CreateTablesRaw {
  // NO MODIFICAR ESTE CÓDIGO


  // Staging Paths
  val inPath = getClass.getResource("/tour_input")
  val csvPathTeams = s"$inPath/teams.csv"
  val csvPathRiders = s"$inPath/riders.json"
  val csvPathStages = s"$inPath/stages.csv"
  val csvPathResult = s"$inPath/result.csv"

  // Get in_data from program params
  val inDate =
    if (args.length == 1) args(0).toInt
    else throw new IllegalArgumentException("Missing input param in_date")

  logger.info(s"in_date: $inDate")

  // 1. Create raw database and tables
  val createRaw = hqlFun.forall(f => executeDDL(f()))

  // 2. Store Raw data
  if (createRaw)
    loadTeams(csvPathTeams, inDate)
      .flatMap(_ => loadRiders(csvPathRiders, inDate))
      .flatMap(_ => loadStages(csvPathStages, inDate))
      .flatMap(_ => loadStages(csvPathResult, inDate)) match {
      case Success(_) => logger.info("Process end Ok")
      case Failure(s) => logger.error(s"Failed. Reason: $s")
    }
}
