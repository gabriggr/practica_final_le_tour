package bluetab.courses.spark

package object practica {
  // Raw Tables
  val SchemaRaw = "tour_raw"
  val TableRawTeams = s"$SchemaRaw.teams"
  val TableRawRiders = s"$SchemaRaw.riders"
  val TableRawStages = s"$SchemaRaw.stages"
  val TableRawResult = s"$SchemaRaw.results"

  // Master
  val SchemaMaster = "tour_master"
  val TableMasterTeams = s"$SchemaMaster.teams"
  val TableMasterRiders = s"$SchemaMaster.riders"
  val TableMasterStages = s"$SchemaMaster.stages"
  val TableMasterResult = s"$SchemaMaster.results"
  val TableMasterTour = s"$SchemaMaster.tablon_tour"

}
